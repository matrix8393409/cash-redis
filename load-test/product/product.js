/*
    Load Testing is primarily concerned with assessing the current performance of your system
    in terms of concurrent users or requests per second.
    When you want to understand if your system is meeting the performance goals,  this is the type of test
    you'll run.

    Run a load test to: 
     - Access the current performance of your system under typical and peak load.
     - Make sure your are continuously meeting the performance standards as you make changes to your system

     Can be used to simulate a normal day in you business

 */
import http from 'k6/http'
import { check, sleep } from "k6";

export let options = {
    insecureSkipTLSVerify: true,
    noConnectionReUse: false,
    stages: [
        { duration: '10s', target: 10000 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes
        { duration: '2m', target: 10000 }, // stay at 'target' users for duration
        { duration: '10s', target: 0 }, // ramp-down to 0 users
    ],
    thresholds: {
        http_req_duration: ['p(99)<1000'], // 99% of requests must complete below 600ms
    }
};

export default () => {
    const url = 'http://0.0.0.0:8080/product/b731b4ba-9dba-4233-9f5c-39d3155e62d1'
    const res = http.get(url)
    check(res, { "status was 200": (r) => r.status == 200 })
    sleep(1); // this is interval that each vus send request
};