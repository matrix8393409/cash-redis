# FROM golang:latest

# WORKDIR /app

# COPY . .

# RUN go mod download

# RUN apk add --no-cache curl

# RUN apk add curl \
#     && go build -o main cmd/main.go \
#     && curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

# CMD ["./main"]


FROM golang:latest AS builder

RUN apt-get update && apt-get install -y curl

WORKDIR /app

COPY . .

RUN go mod download

RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

RUN go build -o main cmd/main.go

CMD ["./main"]
