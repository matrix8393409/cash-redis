package storage

import "app/api/models"

type CacheI interface {
	Close()
	Product() ProductRepoCacheI
	User()   UserRepoCacheI
}

type ProductRepoCacheI interface {
	Add(*models.Product) error
	Get(*models.ProductPrimaryKey) (*models.Product, error)
	Delete(*models.ProductPrimaryKey) error
	Exists(*models.ProductPrimaryKey) (bool, error)
}
type UserRepoCacheI interface {
	Add(*models.User) error
	Get(*models.UserPrimaryKey) (*models.User, error)
	Delete(*models.UserPrimaryKey) error
	Exists(*models.UserPrimaryKey) (bool, error)
}
