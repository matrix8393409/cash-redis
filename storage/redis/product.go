package redis

import (
	"app/api/models"
	"encoding/json"

	"github.com/go-redis/redis"
)
type productRepo struct {
	cachedb *redis.Client
}

func NewProductRepo(redisDB *redis.Client) *productRepo {
	return &productRepo{
		cachedb: redisDB,
	}
}

func (c *productRepo) Add(req *models.Product) error {

	body, err := json.Marshal(req)
	if err != nil {
		return err
	}

	err = c.cachedb.Set(req.Id, body, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *productRepo) Get(req *models.ProductPrimaryKey) (*models.Product, error) {

	var resp *models.Product

	data, err := c.cachedb.Get(req.Id).Result()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(data), &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *productRepo) Delete(req *models.ProductPrimaryKey) error {

	err := c.cachedb.Del(req.Id).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *productRepo) Exists(id *models.ProductPrimaryKey) (bool, error) {

	rowsAffected, err := c.cachedb.Exists(id.Id).Result()
	if err != nil {
		return false, err
	}

	if rowsAffected <= 0 {
		return false, nil
	}

	return true, nil
}
