package redis

import (
	"app/api/models"
	"encoding/json"

	"github.com/go-redis/redis"
)
type userRepo struct {
	cachedb *redis.Client
}

func NewUserRepo(redisDB *redis.Client) *userRepo {
	return &userRepo{
		cachedb: redisDB,
	}
}

func (c *userRepo) Add(req *models.User) error {

	body, err := json.Marshal(req)
	if err != nil {
		return err
	}

	err = c.cachedb.Set(req.Id, body, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *userRepo) Get(req *models.UserPrimaryKey) (*models.User, error) {

	var resp *models.User

	data, err := c.cachedb.Get(req.Id).Result()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(data), &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *userRepo) Delete(req *models.UserPrimaryKey) error {

	err := c.cachedb.Del(req.Id).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *userRepo) Exists(id *models.UserPrimaryKey) (bool, error) {

	rowsAffected, err := c.cachedb.Exists(id.Id).Result()
	if err != nil {
		return false, err
	}

	if rowsAffected <= 0 {
		return false, nil
	}

	return true, nil
}
