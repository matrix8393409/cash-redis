
# How to run after you clone
1. Configurate PostgreSQL configs in makefile and config/config.go
2. Just run the following commands:

```
make migration-down
make migration-up
make run
```

# Check if you program is running well
1. Enter the following url to web browser. `http://localhost:8080`
